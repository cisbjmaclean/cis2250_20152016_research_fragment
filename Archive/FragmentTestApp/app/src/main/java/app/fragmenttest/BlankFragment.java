package app.fragmenttest;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BlankFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //get view
        View view = inflater.inflate(R.layout.blank_fragment, container, false);

        //check if data passed
        if(getArguments() != null) {

            //get data and TextView
            Boolean aBoolean = getArguments().getBoolean("aBoolean");
            TextView textView = (TextView) view.findViewById(R.id.textViewFrag);

            //update TextView
            if(aBoolean) {
                textView.setVisibility(View.VISIBLE);
            }
        }

        //return the view
        return view;
    }

    //sets data when needed
    public static BlankFragment newInstance() {

        //create object
        BlankFragment newFragment = new BlankFragment();

        //create bundle to add data
        Bundle args = new Bundle();
        args.putBoolean("aBoolean", true);

        //assign bundle to fragment
        newFragment.setArguments(args);

        //pass back object
        return newFragment;
    }
}
