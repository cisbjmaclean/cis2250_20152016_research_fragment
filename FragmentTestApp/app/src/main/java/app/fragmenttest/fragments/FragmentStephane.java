package app.fragmenttest.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentStephane.OnFragmentStephaneInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentStephane#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentStephane extends Fragment {

    private TextView textViewCurrentDate = null;
    private SeekBar seekBarTextSize = null;

    private OnFragmentStephaneInteractionListener mListener;

    public FragmentStephane() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment FragmentStephane.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance() {
        FragmentStephane fragment = new FragmentStephane();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stephane, container, false);

        textViewCurrentDate = (TextView) view.findViewById(R.id.textViewStephaneDate);
        seekBarTextSize = (SeekBar) view.findViewById(R.id.seekBarStephaneTextSize);

        final int defaultSize = 20;
        final int maxSize = 70;

        //String dateString = "Today's Date is: " + Calendar.MONTH + " " + Calendar.DAY_OF_MONTH + ", " + Calendar.YEAR;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM, d");
        String dateString = ("Today's Date is: " + format.format(calendar.getTime()));
        textViewCurrentDate.setText(dateString);
        textViewCurrentDate.setTextSize(defaultSize);

        seekBarTextSize.setProgress(defaultSize);
        seekBarTextSize.setMax(maxSize);

        seekBarTextSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int progressChanged = defaultSize;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;
                textViewCurrentDate.setTextSize(progressChanged);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                textViewCurrentDate.setTextColor(Color.RED);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                textViewCurrentDate.setTextColor(Color.BLUE);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentStephaneInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentStephaneInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentStephaneInteractionListener {
        // TODO: Update argument type and name
        void onFragmentStephaneInteraction(String message);
    }
}