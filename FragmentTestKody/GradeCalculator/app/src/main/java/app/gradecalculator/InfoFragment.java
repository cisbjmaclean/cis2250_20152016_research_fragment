package app.gradecalculator;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * @author Kody Duncan
 * @since 1/15/2016
 * <p/>
 * When called creates a view with deliverable information.
 * Allows user to input grades for deliverable and updates the ArrayList of deliverables.
 */

public class InfoFragment extends Fragment {

    private int currentIndex = -1;
    private ArrayList<Deliverable> deliverables;
    private Deliverable deliverable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //get selected item data
        currentIndex = getArguments().getInt("index");
        deliverables = (ArrayList<Deliverable>) getArguments().getSerializable("deliverables");
        deliverable = deliverables.get(currentIndex);

        //create a ScrollView to hold content
        ScrollView scroller = new ScrollView(getActivity());
        LinearLayout newLayout = new LinearLayout(getActivity());

        //display data
        TextView textView = new TextView(getActivity());
        final EditText gradeText = new EditText(getActivity());
        Button gradeButton = new Button(getActivity());

        //set view attributes
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = 1.0f;
        params.gravity = Gravity.CENTER;
        newLayout.setLayoutParams(params);
        newLayout.setOrientation(LinearLayout.VERTICAL);
        newLayout.setGravity(Gravity.CENTER);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(5, 5, 5, 5);
        textView.setTextSize(25);
        textView.setId(currentIndex);
        gradeText.setPadding(5, 5, 5, 5);
        gradeText.setInputType(InputType.TYPE_CLASS_NUMBER);
        gradeText.setId((currentIndex + 1));
        gradeButton.setPadding(5, 5, 5, 5);

        //check if grade data was entered
        if (deliverable.getGrade() == -1) {
            //add data to views
            final String data = "\nName: " + deliverable.getName() + "\nWeight: " + deliverable.getWeight() + "\nGrade: ";
            textView.setText(data);
            gradeText.setHint(R.string.enter_grade);
            gradeButton.setText(R.string.calculate_button);

            //set button
            gradeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //get view
                    EditText gradeText = (EditText) getActivity().findViewById((currentIndex + 1));

                    //get input
                    String gradeInput = String.valueOf(gradeText.getText());

                    //set Toast for errors
                    Toast newToast;

                    //make sure input not empty
                    if (!gradeInput.isEmpty()) {
                        double input = Double.parseDouble(gradeInput);
                        if (input <= 100 && input >= 0) { //makes sure grade between 0 and 100
                            double grade = deliverable.getWeight() * (input / 100);
                            deliverables.get(currentIndex).setGrade(grade);
                            Intent newIntent = getActivity().getIntent();
                            newIntent.putExtra("deliverables", deliverables);
                            getActivity().finish();
                            startActivity(newIntent);
                        } else {
                            newToast = Toast.makeText(getActivity(), "Grade must between 0 and 100.", Toast.LENGTH_LONG); //set toast message
                            newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set toast location
                            newToast.show(); //pop toast
                        }
                    } else {
                        newToast = Toast.makeText(getActivity(), "Grade must have value.", Toast.LENGTH_LONG); //set toast message
                        newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set toast location
                        newToast.show(); //pop toast
                    }
                }
            });

            //add to view
            scroller.addView(newLayout);
            newLayout.addView(textView);
            newLayout.addView(gradeText);
            newLayout.addView(gradeButton);
        } else {

            //set decimal formatter
            DecimalFormat decimalFormat = new DecimalFormat("#.#");

            //add data to views
            String data;
            if(deliverable.getName().equalsIgnoreCase("Course Marks")) {
                data = "\nName: " + deliverable.getName() + "\nMark: " + decimalFormat.format(deliverable.getWeight());
            } else {
                data = "\nName: " + deliverable.getName() + "\nWeight: " + deliverable.getWeight() + "\nGrade: " + decimalFormat.format(deliverable.getGrade());
            }
            textView.setText(data);

            //add to view
            scroller.addView(newLayout);
            newLayout.addView(textView);
        }

        //return view
        return scroller;
    }

    //sets data to allow use for fragment side view
    public static InfoFragment newInstance(int index, ArrayList<Deliverable> deliverables) {

        //create object
        InfoFragment newInfo = new InfoFragment();

        //create bundle to add data
        Bundle args = new Bundle();
        args.putSerializable("deliverables", deliverables);
        args.putInt("index", index);

        //assign bundle to fragment
        newInfo.setArguments(args);

        //pass back object
        return newInfo;
    }

    //default getters and setters
    public int getCurrentIndex() {
        return currentIndex;
    }
}
