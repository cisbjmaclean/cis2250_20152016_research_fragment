package app.fragmenttest.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import app.fragmenttest.R;

public class FragmentTristan extends Fragment{

    Button button;
    Spinner spinner;
    TextView text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        //Getting the root view
        View rootView = inflater.inflate(R.layout.fragment_tristan, container, false);
        button = (Button) rootView.findViewById(R.id.roll_button);
        spinner = (Spinner) rootView.findViewById(R.id.dice_spinner);
        text = (TextView) rootView.findViewById(R.id.outcome_text);

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEST", "The button has been pressed");
                int totalSides = Integer.parseInt(spinner.getSelectedItem().toString());
                int result = rollDice(totalSides);
                text.setText("You rolled a " + result);

            }
        });

        return rootView;
    }

    /*
    Purpose: This will return a random int based on the number of sides on a dice.
    Author: Tristan Chatman
    Date: Jan 18 2016
     */
    public int rollDice(int sides){
        Random random = new Random();
        int roll = random.nextInt(sides) + 1;
        return roll;
    }


}
