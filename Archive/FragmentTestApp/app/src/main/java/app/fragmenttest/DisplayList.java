package app.fragmenttest;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import app.fragmenttest.fragments.FragmentChrisM;
import app.fragmenttest.fragments.FragmentChrisS;
import app.fragmenttest.fragments.FragmentJeffG;
import app.fragmenttest.fragments.FragmentJeffN;
import app.fragmenttest.fragments.FragmentKyle;
import app.fragmenttest.fragments.FragmentNotImplemented;

public class DisplayList extends AppCompatActivity implements FragmentJeffG.OnFragmentJeffGInteractionListener, AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //create a fragment transaction
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();


        //checks if DeliverableFragment or InfoFragment is being called
        if(getIntent().getIntExtra("position", -1) == -1) {

            //set activity using xml layout file
            setContentView(R.layout.activity_list);

            //get list view and set as clickable
            ListView listView = (ListView) findViewById(R.id.names);
            listView.setOnItemClickListener(this);
        } else {
            //only called if Activity is active and not terminated
            if (savedInstanceState == null) {

                //get position
                int position = getIntent().getIntExtra("position", 0);

//                //change display according to index
            }

            //destroy view if in landscape mode and reset
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                finish();
                getIntent().putExtra("index", -1);
                startActivity(getIntent());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //default return value
        boolean itemSelected = super.onOptionsItemSelected(item);

        //create Toast for errors
        Toast newToast;

        //get menu item and perform action required
        switch (item.getItemId()) {
            case R.id.restart:
                itemSelected = true;
                finish();
                startActivity(new Intent(this, MainActivity.class));
                newToast = Toast.makeText(getApplicationContext(), "Restarting App", Toast.LENGTH_SHORT); //set Toast Message
                newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set Toast location
                newToast.show(); //pop Toast
                break;
            default:
                Log.d("ERROR", "Issue with menu options");
                break;
        }

        //return value
        return itemSelected;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //create a fragment transaction
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

//        //change view depending on orientation
//        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            //display fragment
            switch (position) {
                case 0:
                    Log.d("TEST", "Displaying BlankFragment.");
                    ft.replace(R.id.layout_list, new BlankFragment(), "fragment").commit();
                    break;
                case 1:
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_list, BlankFragment.newInstance(), "fragment").commit();
                    break;
                case 9:
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_list, new FragmentJeffG(), "fragment").commit();
                    break;
                case 14:
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_list, new FragmentChrisM(), "fragment").commit();
                    break;
                case 18:
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_list, new FragmentJeffN(), "fragment").commit();
                    break;
                case 19:
                    Log.d("TEST", "Displaying kyles fragment.");
                    ft.replace(R.id.layout_list, new FragmentKyle(), "fragment").commit();
                    break;
                case 23:
                    Log.d("TEST", "Displaying Fragment with Text.");
                    ft.replace(R.id.layout_list, new FragmentChrisS(), "fragment").commit();
                    break;
                default:
                    Log.d("ERROR", "Current Index not found.");
                    ft.replace(R.id.layout_list, new FragmentNotImplemented(), "fragment").commit();
                    break;
            }
//        } else {
//            Intent newIntent = new Intent(this, DisplayList.class);
//            newIntent.putExtra("position", position);
//            startActivity(newIntent);
//        }
    }
    @Override
    public void onFragmentJeffGInteraction(String message) {
        Log.d("2250", "in onFragmentJeffGInteraction  activity received from fragment"+message);
        Toast.makeText(this, "interacted with Jeff fragment"+message, Toast.LENGTH_SHORT).show();
    }

}
