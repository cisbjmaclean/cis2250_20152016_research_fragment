# README #

### What is this repository for? ###

This repository is used to hold the various fragments to be created by cis2250 students.  Everyone should use their imagination to think up a small one page application which has a couple widgets and uses a layout other than relative (try other than linear too!).  Make a fragment that can be launched using this Android application.  Get the fragments working in your own copy of this application and we will get everything combined on Tuesday.  

### How do I get set up? ###

Clone this repository and open in Android Studio 1.5.1

### Who do I talk to? ###

* BJ MacLean