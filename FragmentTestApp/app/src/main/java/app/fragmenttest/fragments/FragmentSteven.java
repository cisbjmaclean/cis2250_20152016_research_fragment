package app.fragmenttest.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import app.fragmenttest.R;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *  interface
 * to handle interaction events.
 * Use the {@link FragmentSteven#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSteven extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private View view;

    private static ImageView imageView3;
    private static ToggleButton toggleButton2;


    // TODO: Rename and change types of parameters



    public FragmentSteven() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSteven.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSteven newInstance(String param1, String param2) {
        FragmentSteven fragment = new FragmentSteven();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("test", "onCreate: Created");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_steven, container, false);

        imageView3 = (ImageView)view.findViewById(R.id.imageView3);
        toggleButton2 = (ToggleButton)view.findViewById(R.id.toggleButton2);
        Log.d("test","Create View");
        toggleButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {
                if (isChecked) {
                    imageView3.setImageResource(R.drawable.pepsi);
                } else {
                    imageView3.setImageResource(R.drawable.larry);
                }
            }
        }) ;
        return view;
    }


}
