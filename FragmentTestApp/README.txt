PLEASE READ THE MAIN PROJECTS README ON HOW TO SETUP! (ALWAYS UPLOAD/UPDATE DATABASE BEFORE USING IF REQUIRED!)

Quick Overview:

	-Application allows users to select fragments from a list or by swiping (fling).
	
The Fragment Test App has the following updates:

	-Linear, Frame, Fragment, and Relative Layouts
	-Menu Button which allows user to Restart App

Functionality Added:

	-Main Page
		-Which asks user what they want a List or Swipe (fling) function.
	-Swipe Page
		-Allows user to swipe left or right and will create a blank fragment or a fragment with text.
	-List Page
		-Allows user to select a fragment name and will create an blank fragment or a fragment with text in a new screen.
			-If users changes orientation the list and fragment are displayed beside each other.
	-Blank Fragment
		-Used to create a blank fragment or a fragment with text.
	
Warnings/Issues:

	-None Found (But Possible)