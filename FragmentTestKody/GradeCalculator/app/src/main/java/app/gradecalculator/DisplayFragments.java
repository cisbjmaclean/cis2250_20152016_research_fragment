package app.gradecalculator;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

/**
 * @author Kody Duncan
 * @since 1/15/2016
 *
 * When Activity is called creates Fragment views for DeliverableFragment and InfoFragment (dynamically changes according orientation).
 */

public class DisplayFragments extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //checks if DeliverableFragment or InfoFragment is being called
        if(getIntent().getIntExtra("index", -1) == -1) {
            setContentView(R.layout.activity_list); //set activity using xml layout file
        } else {
            //only called if Activity is active and not terminated
            if (savedInstanceState == null) {
                InfoFragment details = new InfoFragment();

                details.setArguments(getIntent().getExtras());

                getFragmentManager().beginTransaction().add(android.R.id.content, details).commit();
            }

            //destroy view if in landscape mode and reset
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                finish();
                getIntent().putExtra("index", -1);
                startActivity(getIntent());
            }
        }
    }

    @Override
    public void onBackPressed() { //resets activity and updates data if required on back press
        super.onBackPressed();
        if(getIntent().getIntExtra("index", -1) != -1) {
            getIntent().putExtra("index", -1);
            startActivity(getIntent());
            finish();
        } else {
            finish();
        }
    }
}
