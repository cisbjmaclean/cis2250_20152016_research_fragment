package app.fragmenttest.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

import app.fragmenttest.R;

/**
 * Created by mmacgregor31749 on 1/19/2016.
 */
public class FragmentMajor extends Fragment implements CompoundButton.OnCheckedChangeListener   {

    ImageView i;
    ToggleButton t;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("BJTEST", "onCreate - Fragment1");

            t = (ToggleButton) t.findViewById(R.id.toggleButton);
            i = (ImageView) i.findViewById(R.id.imageView);

            t.setOnCheckedChangeListener(this);
        }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
        {
            i.setBackgroundColor(Color.YELLOW);
        }
        else
        {
            i.setBackgroundColor(Color.BLACK);
        }
    }

    }

