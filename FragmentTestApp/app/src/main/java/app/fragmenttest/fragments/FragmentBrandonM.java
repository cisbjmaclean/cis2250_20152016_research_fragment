package app.fragmenttest.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import app.fragmenttest.R;

public class FragmentBrandonM extends Fragment {

    public FragmentBrandonM() {

    }

    public static FragmentBrandonM newInstance(String param1, String param2) {
        FragmentBrandonM fragment = new FragmentBrandonM();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_brandon_m, container, false);

        Button btn1 = (Button) view.findViewById(R.id.button1);
        Button btn2 = (Button) view.findViewById(R.id.button2);
        Button btn3 = (Button) view.findViewById(R.id.button3);
        Button btn4 = (Button) view.findViewById(R.id.button4);

        final LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout);

        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Incorrect choice!", Toast.LENGTH_LONG).show();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Incorrect choice!", Toast.LENGTH_LONG).show();
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Correct choice!", Toast.LENGTH_LONG).show();
                layout.setBackgroundColor(0x1DE9B6);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(getActivity(), "That's really wrong!", Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
}
