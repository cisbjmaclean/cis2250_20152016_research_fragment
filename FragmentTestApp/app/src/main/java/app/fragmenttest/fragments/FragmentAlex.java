package app.fragmenttest.fragments;


import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import app.fragmenttest.R;

public class FragmentAlex extends Fragment {
    SeekBar seekBar;
    ImageView imgView;
    Bitmap bitmap;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //get view
        View view = inflater.inflate(R.layout.fragment_alex, container, false);

        //check if data passed
        if(getArguments() != null) {

            //get data and TextView
            Boolean aBoolean = getArguments().getBoolean("aBoolean");
            TextView textView = (TextView) view.findViewById(R.id.textView);
            bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.dog_pic);
            imgView=(ImageView)view.findViewById(R.id.imageView);
            imgView.setImageBitmap(bitmap);
            seekBar= (SeekBar) view.findViewById(R.id.seekBar);
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                    imgView.setColorFilter(setBrightness(progress));
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            //update TextView
            if(aBoolean) {
                textView.setVisibility(View.VISIBLE);
            }
        }

        //return the view
        return view;
    }

    //sets data when needed
    public static FragmentAlex newInstance() {

        //create object
        FragmentAlex newFragment = new FragmentAlex();

        //create bundle to add data
        Bundle args = new Bundle();
        args.putBoolean("aBoolean", true);

        //assign bundle to fragment
        newFragment.setArguments(args);

        //pass back object
        return newFragment;
    }







    public static PorterDuffColorFilter setBrightness(int progress) {
        if (progress >=	100)
        {
            int value = (int) (progress-100) * 255 / 100;

            return new PorterDuffColorFilter(Color.argb(value, 255, 255, 255), PorterDuff.Mode.SRC_OVER);

        }
        else
        {
            int value = (int) (100-progress) * 255 / 100;
            return new PorterDuffColorFilter(Color.argb(value, 0, 0, 0), PorterDuff.Mode.SRC_ATOP);


        }
    }

}
