package app.gradecalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * @author Kody Duncan
 * @since 1/15/2016
 *
 * This is the Main Activity which starts when application is loaded.
 * This activity gets deliverable information and passes that information to an list fragment.
 */

public class MainActivity extends AppCompatActivity {

    private int currentWeight;
    private ArrayList<Deliverable> deliverables = new ArrayList<Deliverable>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //sets layout for Activity
    }

    public void onClickAdd(View view) {
        //declare all EditTexts
        EditText nameInput = (EditText) findViewById(R.id.project_name);
        EditText weightInput = (EditText) findViewById(R.id.project_weight);

        //declare Toast for error handling
        Toast newToast;

        //get EditTexts values
        String name = String.valueOf(nameInput.getText());
        String weightString = String.valueOf(weightInput.getText());

        //verify user input
        if(!name.isEmpty() && !weightString.isEmpty()) {

            //declare deliverable weight
            double weight = Double.parseDouble(weightString);

            //update weight total
            currentWeight += weight;

            //add deliverable dependent on current weight value
            final int maxWeight = 100;
            if(currentWeight < maxWeight) {
                //add to ArrayList
                deliverables.add(new Deliverable(name, weight));
            } else if(currentWeight == maxWeight) {
                //add to ArrayList
                deliverables.add(new Deliverable(name, weight));
                //redirect user to ListFragment
                Intent newActivity = new Intent(this, DisplayFragments.class);
                newActivity.putExtra("deliverables", deliverables);
                startActivity(newActivity);
                //clear data
                deliverables.clear();
                currentWeight = 0;
            } else {
                currentWeight -= weight;
                double max = maxWeight - currentWeight;
                newToast = Toast.makeText(this, "Weight must be " + max + " or less.", Toast.LENGTH_LONG); //set toast message
                newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set toast location
                newToast.show(); //pop toast
            }
        } else {
            newToast = Toast.makeText(this, "Please enter a name and weight.", Toast.LENGTH_LONG); //set toast message
            newToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0); //set toast location
            newToast.show(); //pop toast
        }
    }
}
