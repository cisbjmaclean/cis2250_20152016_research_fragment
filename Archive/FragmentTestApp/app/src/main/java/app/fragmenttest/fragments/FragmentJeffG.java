package app.fragmenttest.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import app.fragmenttest.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentJeffG.OnFragmentJeffGInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentJeffG#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentJeffG extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private TextView switchStatus;
    private TextView toggleStatus;
    private Switch mySwitch;
    private ToggleButton myToggle;
    private LinearLayout main_view;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentJeffGInteractionListener mListener;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentJeffG.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentJeffG newInstance(String param1, String param2) {
        FragmentJeffG fragment = new FragmentJeffG();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentJeffG() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //inflate the fragment view
        View view = inflater.inflate(R.layout.fragment_fragment_jeffg, container, false);
        //match the switch and status string to the elements on the layout
        switchStatus = (TextView) view.findViewById(R.id.textView1);
        mySwitch = (Switch) view.findViewById(R.id.switch1);
        main_view = (LinearLayout) view.findViewById(R.id.main_view);
        //attach the event listener to the switch
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    //if the switch has been enabled
                    main_view.setBackgroundColor(Color.RED);
                    Toast.makeText(getActivity(), "Switch is ON", Toast.LENGTH_SHORT).show();

                } else {
                    // The switch is disabled
                    main_view.setBackgroundColor(Color.YELLOW);
                    Toast.makeText(getActivity(), "Switch is OFF", Toast.LENGTH_SHORT).show();

                }
            }
        });


//        //match the toggle and status string to the elements on the layout
        toggleStatus = (TextView) view.findViewById(R.id.textView2);
        myToggle = (ToggleButton) view.findViewById(R.id.toggleButton1);
        //attach the event listener to the toggle
        //bjm
        myToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {


            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mListener.onFragmentJeffGInteraction("test from fragment");
                if (isChecked) {
                    //if the toggle has been enabled
//                    toggleStatus.setText("Toggle is ON");
                    main_view.setBackgroundColor(Color.CYAN);
                    Toast.makeText(getActivity(), "Toggle is ON", Toast.LENGTH_SHORT).show();


                } else {
                    // The toggle is disabled
//                    toggleStatus.setText("Toggle is OFF");
                    main_view.setBackgroundColor(Color.MAGENTA);
                    Toast.makeText(getActivity(), "Toggle is OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentJeffGInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentJeffGInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentJeffGInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentJeffGInteraction(String message);
    }


}
